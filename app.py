
from flask import Flask, render_template, redirect, url_for, request
from flask_jsglue import JSGlue
import openResume

app = Flask(__name__)

jsglue = JSGlue(app)

openResume.fill_with_test_data()


@app.route("/", methods=["GET"])
def login():
    return render_template("login.html")


@app.route("/user-login/<string:account>", methods=["POST"])
def user_login(account):
    if openResume.user_exists(account):
        openResume.set_default_account(account)
        return "/user/"
    else:
        return "ERROR", 400


@app.route("/business-login/<string:account>", methods=["POST"])
def business_login(account):
    if openResume.company_exists(account):
        openResume.set_default_account(account)
        return "/business/"
    else:
        return "ERROR", 400


@app.route("/register-business/", methods=["POST"])
def register_business():
    openResume.set_default_account(request.form["address"])
    openResume.add_company(request.form["id"], request.form["name"], request.form["field"])
    return redirect(url_for("business"))


@app.route("/register-user/", methods=["POST"])
def register_user():
    openResume.set_default_account(request.form["address"])
    openResume.add_user(request.form["id"], request.form["name"], request.form["surname"], request.form["birthdate"],
                        request.form["phone"], request.form["email"], request.form["residency"])
    return redirect(url_for("user"))


@app.route("/user/", methods=["GET"])
def user():
    user = openResume.get_user()
    return render_template("user.html", user=user, references=user.references)


@app.route("/business/", methods=["GET"])
def business():
    business = openResume.get_company()
    return render_template("business.html", business=business, references=business.references)


@app.route("/check/<string:account>", methods=["POST"])
def check_account_existence(account):
    if openResume.user_exists(account) or openResume.company_exists(account):
        return "ERROR",400
    else:
        return "SUCCESS",200


@app.route("/change-visibility/<string:reference_id>", methods=["POST"])
def change_notes_visibility(reference_id):
    openResume.change_notes_visibility(reference_id)
    return redirect(url_for("user"))


@app.route("/add-experience/", methods=["POST"])
def add_experience():
    current = ""
    if request.form.getlist('current'):
        current = True
    else:
        current = False
    openResume.add_reference(request.form["company_address"], request.form["title"], request.form["period"],
                            request.form["description"], False, current)
    return redirect(url_for("user"))


@app.route("/add-education/", methods=["POST"])
def add_education():
    current = ""
    if request.form.getlist('current'):
        current = True
    else:
        current = False
    openResume.add_reference(request.form["company_address"], request.form["title"], request.form["period"],
                               request.form["description"], True, current)
    return redirect(url_for("user"))


@app.route("/disconfirm-reference/<string:reference_id>", methods=["GET"])
def disconfirm_reference(reference_id):
    openResume.disconfirm_reference(reference_id)
    return redirect(url_for("business"))


@app.route("/confirm-reference/", methods=["POST"])
def confirm_reference():
    openResume.confirm_reference(request.form["id"], request.form["notes"])
    return redirect(url_for("business"))


@app.route("/user/<string:address>", methods=["GET"])
def user_public(address):
    user = openResume.get_user_public(address)
    return render_template("public-user.html", user=user, references=user.references)


@app.route("/update-period/", methods=["POST"])
def update_period():
    openResume.update_period(request.form["id"], request.form["period"])
    return redirect(url_for("user"))


@app.route("/logout/", methods=["GET"])
def logout():
    return redirect(url_for("login"))


if __name__ == "__main__":
    app.run()
