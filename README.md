
Per eseguire, dopo aver scaricato il repository:

## 1) Scaricare: 

1. GANACHE CLI 
    https://www.trufflesuite.com/ganache 
2. METAMASK (Estensione di Google Chrome)
    https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn

---

## 2) Avviare Ganache:

1. Aprire il programma
2. Cliccare su QUICKSTART

---

## 3) Impostare correttamente Metamask:

1. Creare un nuovo Wallet
2. Nella pagina principale che appare dopo aver creato il Wallet, in alto a destra cliccare dove c'è scritto Rete Ethereum Principale. 
3. Selezionare RCP Personalizzata.
4. Nel campo "Nuovo URL RPC" incollare l'url HTTP://127.0.0.1:7545 (Controllare che sia uguale all'url indicato su Ganache in alto sotto RPC SERVER)

5. Nella pagina principale, cliccare sul cerchietto in alto a destra ed entrare nelle impostazioni.
6. Selezionare "Connessioni" e aggiungere "http://localhost:5000/" ai siti autorizzati.

---

## 4) Importare alcuni account su Metamask:

Tenendo presente che nell'applicazione alcuni account sono già utilizzati per i dati di test, in particolare:



 *  il numero 9, per il deploy dello smart contract (0xb549a960D00a7d6e451F976D3Aa5Ab1b13449b18)
 *  il numero 8, come account di test per "E-Corp" (0xE683D65d98f54CF6388f1140C2F121D3DA2cF62D)
 *  il numero 7, come account di test per "Università di Bologna" (0x9125EF4E969B5085F7D7e75d031F797275895540)
 *  il numero 6, come account di test per "Giulia Rossi" (0x6D79F79Bf3Cb92370547a39452De1f673F85175c)
 *  il numero 5, come account di test per "Matteo Bianchi" (0xB316b3499e0B08be0689CEFF5D96b601c7506D89)

Utilizzando questi account precaricati è possibile testare le funzionalità dell'applicazione saltando l'inserimento dati.


1. Su Ganache scegliere l'account da importare e cliccare sul simbolo della chiave a destra
2. Copiare la Private Key
3. Su Metamask cliccare il cerchietto, quindi "Importa Account" e incollare la Private Key
4. Ripetere con gli account che si desidera importare 

---

## 5) Eseguire da terminale (MAC/LINUX):

1. Posizionarsi nella cartella del progetto (chiamata "open-resume")
2. Eseguire `source venv/bin/activate`
3. Eseguire `python app.py`
4. A questo punto l'applicazione sarà in esecuzione al link http://127.0.0.1:5000/ e vi si potrà accedere da browser (in questo caso Chrome, per l'utilizzo di Metamask)

Tenere a mente che ogni operazione viene eseguita sulla base dell'account al quale si è fatto login su Metamask.

