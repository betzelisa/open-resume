
function userLogin() {
    initWeb3Login("user");
}

function businessLogin() {
    initWeb3Login("business");
}

function userRegister() {
    initWeb3Register("user");
}

function businessRegister() {
    initWeb3Register("business");
}

function getLink(){
    getUserLink();
}


function initWeb3Login(type) {
    if (typeof web3 !== 'undefined') {
        // Get the web3 instance provided by MetaMask.
        web3 = new Web3(web3.currentProvider);
        //get the account number from MetaMask
        web3.eth.getCoinbase(function(err, account) {
            if (err === null && account != null) {
                console.log(account);
                $.ajax({
                    type: 'POST',
                    url:  Flask.url_for(type+"_login", {"account": account}),
                    success:function(response){  window.location.href = response;},
                    error:function(){
                        document.getElementById('errorlogintext').innerHTML = "Account " + account + " not registered.";
                        document.getElementById('errorlogin').style.display= 'block';
                    }
                })
            }
            else {
                alert("Make sure you're logged-in to MetaMask!");
            }
        });
    }
    else {
        alert("Make sure you're logged-in to MetaMask!");
    }
}

function initWeb3Register(type) {
    if (typeof web3 !== 'undefined') {
        // Get the web3 instance provided by MetaMask.
        web3 = new Web3(web3.currentProvider);
        //get the account number from MetaMask
        web3.eth.getCoinbase(function(err, account) {
            if (err === null && account != null) {
                console.log(account);
                $.ajax({
                    type: 'POST',
                    url:  Flask.url_for("check_account_existence", {"account": account}),
                    success:function(response){
                        document.getElementById(type+'-eth').value = account;
                        document.getElementById(type+'errorregister').style.display= 'none';
                        document.getElementById(type+'registerbutton').disabled = false;
                    },
                    error:function(){
                        document.getElementById(type+'-eth').value = account;
                        document.getElementById(type+'errorregister').style.display= 'block';
                        document.getElementById(type+'registerbutton').disabled = true;
                    }
                })

            }
            else {
                alert("Make sure you're logged-in to MetaMask!");
            }
        });
    }
    else {
        alert("Make sure you're logged-in to MetaMask!");
    }
}

function getUserLink() {
    if (typeof web3 !== 'undefined') {
        web3.eth.getCoinbase(function(err, account) {
            if (err === null && account != null) {
                document.getElementById('link').value = "http://localhost:5000/user/" + account;
            }
        })
    }
}

function copyLink(){
    var copyText = document.getElementById('link');
    copyText.select();
    document.execCommand("copy");
    document.getElementById('copied').innerHTML = "Copied!"
}
